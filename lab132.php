<?php
/**
 * Created by PhpStorm.
 * User: Alexander Kunup
 * Date: 02.02.2017
 * Time: 19:13
 */
$number_of_items = mt_rand(0,20); // generate a random value
$number_of_items = 1; // for testing

$item_name = $number_of_items == 1 ? " apple" : " apples"; // if 1 apple then  $item_name = 'apple'
echo ($number_of_items  == 0 ? "no" : $number_of_items ).$item_name;  // if 0 apple then  appear 'no' instead of  $number_of_items

echo "\n-----Another way to solve this task-------------\n";

$wordSuffix = ($number_of_items == 1) ? '' : 's';
echo ($number_of_items  == 0 ? "no" : $number_of_items)." apple{$wordSuffix} ";

