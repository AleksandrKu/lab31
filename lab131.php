<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 02.02.2017
 * Time: 15:24
 */
// lab 1.3.1
$weekDay = date ('l');
//$currentHour = date('H');
$currentHour = 1;  // set up hour for testing
$startWorkTime = 9;
$endWorkTime = 18;

 echo "Hello. Today {$weekDay}. Now is {$currentHour}  hour(s).\n";
if ($weekDay == 'Saturday' || $weekDay == 'Sunday' ) {
        // weekend
    echo "It's weekend!!!";
} elseif ($currentHour >= $startWorkTime && $currentHour <= $endWorkTime) {
        // Middle of the work day
        $leftHourWork = $endWorkTime - $currentHour;
        echo "It's {$leftHourWork} hour(s)  to the end of the work.";
} elseif ($currentHour <= $startWorkTime ){
    // How many hours to sart work
    $leftHourToWork = $startWorkTime - $currentHour;
    echo "You have to be at work after {$leftHourToWork} hour(s).";
} elseif ($currentHour >= $endWorkTime ) {
    //  How many hours after work
    $afterHourWork = $currentHour - $endWorkTime;
    echo "You should relax {$afterHourWork} hour(s) after work";
} else {
    echo "Error! Something is wrong with time!";
}