<?php

function printHelloMessage() {
    echo "Hello! Welcome again!";
}

function printGoodbayMessage() {
    echo "Bye! Have a nice day!";
}

$isWelcome = (boolean)mt_rand(0,1);
if ($isWelcome) {
    printHelloMessage();
} else {
    printGoodbayMessage();
}
var_dump($isWelcome);