<?php
$delimiter = '---------------';

function drawDelimiter()
{
    global $delimiter;
    echo $delimiter . PHP_EOL;
}

$content = 'Content string';

$drawContentString = function () use ($content) {
    echo $content . PHP_EOL;
};

drawDelimiter();
$drawContentString();
drawDelimiter();
