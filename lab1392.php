<?php
//   printing line of numbers
function recursiveCounter($number, $end)
{
    echo $number . ' ';
    return $number < $end ? recursiveCounter($number + 1, $end) : null;
}
$start = 0; // number for the start
$finish = 20; // number for the end
recursiveCounter($start,$finish);
