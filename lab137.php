<?php

function calculateSum()
{
    $args = func_get_args();
    $result = array_sum($args);
    foreach ($args as $argument) {
        echo $argument." ";
    }
  echo "= ".$result;
}

function multiply ($a, $b)
{
    echo $a + $b;
}

echo "Sum: ";
calculateSum(mt_rand(1,5), mt_rand(1,5), mt_rand(1,5), mt_rand(1,5));
echo PHP_EOL;

echo "Multiplication: ";
multiply (mt_rand(1,5), mt_rand(1,5));
echo"\n-----1.3.7 Lab_Resolution ------\n";

function calculateSum2(...$numbers)
{
    echo implode(' + ', $numbers) . ' = ' . array_sum($numbers) . PHP_EOL; // implode () - объеденяет элементы массива встроку
}
calculateSum2(1, 2, 3, 4, 5);

function multiply2($firstNumber, $secondNumber)
{
    $result = $firstNumber * $secondNumber;
    echo $firstNumber . ' * ' . $secondNumber . ' = ' . $result . PHP_EOL;
}
$firstNumber = mt_rand(0,10);
$secondNumber = mt_rand(0, 10);
multiply2($firstNumber, $secondNumber);
