<?php
// raised to the power
function degree ($x,$y) {
    if ($y) {
        return $x * degree ($x, $y - 1);
    }
    return 1;
}
$d = 2;
$d2 = 4;
echo (degree($d,$d2));
echo PHP_EOL;
echo PHP_EOL;

//  calculation factorial
function calculateFactorial($n) {
    if(!$n) return 1;
    return $n * calculateFactorial($n-1);
}
$factorial = mt_rand(1,5);

echo " Factorial $factorial! = ".calculateFactorial($factorial); // 1
echo PHP_EOL;


