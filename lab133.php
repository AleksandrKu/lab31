<?php
/**
 * Created by PhpStorm.
 * User: Aleksander
 * Date: 03.02.2017
 * Time: 13:54
 */
$dayId = date('w');

switch ($dayId) {
    case 1:
        echo 'It is Monday today';
        break;
    case 2:
        echo 'It is Tuesday today';
        break;
    case 3:
        echo 'It is Wednesday today';
        break;
    case 4:
        echo 'It is Thursday today';
        break;
    case 5:
        echo 'It is Friday today';
        break;
    default:
        echo 'Weekend';
}